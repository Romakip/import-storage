<?php

//include "MultipleLoadingEad.php";

class Import {

    private static $path = null;
    private static $countLineDescriptionFile = 1;
    private static $countLineFile = 1;
    private static $pathToDescrFile = null;
    private static $pathToRelationFile = null;
    private static $findMainDoc = false;

    private static $absPathToDir;
    private static $absPathToEdDescr;
    private static $absPathToEdFiles;
    private static $url;
    private static $edDescr; # Массив при чтении файла ED_DESCR.csv
    private static $edFiles; # Массив при чтении файла ED_Files.csv
    private static $eadArray; # Общий  массив

    private static $sourceID; # Получили айди источника
    private static $statusEad; # Получили айди источника
    private static $edStatusAccepted; # Получили айди источника
    private static $edStatusUpdated; # Получили айди источника
    private static $fileRoles;
    private static $fileRoleTypeDocument;

    private static $parentFilesOnEd;
    private static $hasRoleFiles;

    private static $resultData;


    /**
     * @return string|true
     * Старт приложения
     */
    public static function startImport($path, $url)
    {
        if (!file_exists($path)) {
            self::writeLogs("Каталог по указанному пути: '$path' не существует", 404);
        } else {
            self::$path = $path;
            self::$url = $url;
            file_put_contents('ED_DESCR.csv', '');
            file_put_contents('ED_Files.csv', '');
            $elements[] = self::$path;
            self::getHierarchy($elements);
            self::end();

            self::defaultEad(self::$path, self::$url);
        }
        echo "Скрипт завершил свою работу!";
    }

    /**
     * @param $fileTxt
     * Конвертирует файл txt в csv
     */
    private static function parseFileTxt($fileTxt, $pathFolder)
    {
        $content = file_get_contents($fileTxt);
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16LE');
        self::explodeTxtFile($content, self::getDocumentPaths($pathFolder));
    }

    /**
     * @param $folderPath
     * @return array
     * Возвращает массив путей документов
     */
    private static function getDocumentPaths($folderPath)
    {
        $result = array();
        $paths = self::addAbsolutePath($folderPath);
        $paths = self::deleteBaseDirectory($paths);
        foreach ($paths as $path) {
            if (!preg_match('/(RC_Data_)/', $path[0])) {
                $result[] = $path[0];
            }
        }
        if (count($result) < 1) {
            self::writeLogs('В каталоге нет документов, принадлежащих к ЭД, '.'путь к каталогу: '.$folderPath);
        }
        return $result;
    }

    private static function writeLogs($msg, $type = 'ERROR') {
        $date =  date_format(date_create(), 'Y-m-d H:i:s');
//        file_put_contents('upload-log.log', "{$date} - {$type} - {$msg} ".PHP_EOL);
        $fp = fopen('upload-log.log', 'a');
        fwrite($fp, "{$date} - {$type} - {$msg} "."\n");
        fclose($fp);
    }

    /**
     * @param $content
     * Получает контент в виде строки, разбивает его построчно и формирует массив каждого заголовка,
     * затем передает данные функции пополнения файла csv
     * Получает путь к дирректории, где был найден основной txt-файл для того,
     * чтобы найти в этом каталоге все документы, принадлежащие к ЭД
     */
    private static function explodeTxtFile($content, $documentPaths)
    {
        $resultDescr = array();
        $content = explode("\n\r\n", $content);
        $resultDescr['Номер строки'] = mb_convert_encoding(self::$countLineDescriptionFile, 'windows-1251');
        if (preg_match('/Регистрационный номер:/', $content[0])) {
            $register = trim(explode('Регистрационный номер:', $content[0])[1]);
            //$resultDescr['Номер документа'] = mb_convert_encoding(trim(explode('от', $register)[0]), 'windows-1251');
            $resultDescr['Номер документа'] = mb_convert_encoding(trim(explode('от', $register)[0]), 'windows-1251');
            //$resultDescr['Дата регистрации'] = mb_convert_encoding(trim(explode('от', $register)[1]), 'windows-1251');
            $resultDescr['Дата регистрации'] = mb_convert_encoding(trim(explode('от', $register)[1]), 'windows-1251');
        }
        if (preg_match('/Содержание:/', $content[5])) {
            $description = trim(explode('Содержание:', $content[5])[1]);
            $description = str_replace("\n", " ", $description);
            $description = str_replace("\t", " ", $description);
            $description = str_replace("\r", " ", $description);
            $resultDescr['Содержание'] = mb_convert_encoding($description, 'windows-1251');
        }
        self::pullDescriptionCsv($resultDescr); //заполняет файл-описание
        $resultFiles['Номер строки'] = self::$countLineFile;
        $resultFiles['Номер строки ЭД из файла-описания'] = self::$countLineDescriptionFile;
        $resultFiles['Пути документов'] = $documentPaths;
        //self::pullFilesCsv($resultFiles, trim(explode('от', $register)[0])); //заполняет файл связей
        self::pullFilesCsv($resultFiles, trim(explode('от', $register)[0])); //заполняет файл связей

    }

    /**
     * Заполняет файл связей документов
     */
    private static function pullFilesCsv($content, $nameMainDocum)
    {
        foreach ($content['Пути документов'] as $itemPathToDocument) {
            $data['Текущая строка в документе'] = mb_convert_encoding(self::$countLineFile, 'windows-1251');
            $data['Строка документа из файла-описания'] = mb_convert_encoding(self::$countLineDescriptionFile-1, 'windows-1251');
            $data['Путь к документу'] = mb_convert_encoding($itemPathToDocument, 'windows-1251');
            $data['Роль'] = mb_convert_encoding(self::checkMainDocum($itemPathToDocument, $nameMainDocum) ?
                'Документ' : 'Приложение', 'windows-1251');
            $file = fopen('ED_Files.csv', 'a+');
            fputcsv($file, $data, ';');
            fclose($file);
            self::$countLineFile++;
        }
        self::$findMainDoc = false;
    }

    /**
     * Проверяет, является ли текущий документ главным
     */
    private static function checkMainDocum($pathDocument, $nameMainDocument)
    {
        if (!self::$findMainDoc) {
            if (explode('.', explode('/', $pathDocument)[count(explode('/', $pathDocument))-1])[0] === $nameMainDocument ) {
                self::$findMainDoc = true;
                return true;
            } else {
                $partParthDoc = explode('.', explode('/', $pathDocument)[count(explode('/', $pathDocument))-1])[0];
                $result = (stripos($partParthDoc, $nameMainDocument) === 0);
                self::$findMainDoc = $result;
                return $result;
            }
        }
    }

    /**
     * Speed find main doc in array
     */
    private function speedFindMainDoc($allPaths, $nameMainDocument)
    {
        $result = false;
        foreach ($allPaths as $pathDocument) {
            if (explode('.', explode('/', $pathDocument)[count(explode('/', $pathDocument))-1])[0] === $nameMainDocument) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @param $content
     * Заполняет файл-описание
     */
    private static function pullDescriptionCsv($content)
    {
        $file = fopen('ED_DESCR.csv', 'a+');
        fputcsv($file, $content, ';');
        fclose($file);
        self::$countLineDescriptionFile++;
    }

    /**
     * Ищет основной txt файл, который будет переводиться в csv
     */
    private static function searchMainTxtFile($catalog, $pathFolder)
    {
        foreach ($catalog as $itemCatalog) {
            if (is_array($itemCatalog)) {
                self::searchMainTxtFile($itemCatalog, $pathFolder);
            } else {
                if (!is_dir($itemCatalog) && stripos($itemCatalog, '.txt')) {
                    if(self::checkNameTxtFile($itemCatalog, $pathFolder)) {
                        self::parseFileTxt($itemCatalog, $pathFolder);
                    }

                }
            }
        }
    }

    /**
     * @param $elements
     * @return mixed
     * Получает полную иерархию переданного каталога
     */
    private static function getHierarchy($elements)
    {
        foreach ($elements as $key => $element) {
            if (is_array($element)) {
                $elements[$key] = self::getHierarchy($element);
            }
            else {
                if (is_dir($element)) {
                    $elements[] = self::addAbsolutePath($element);
                    $elements[(count($elements)-1)] = self::deleteBaseDirectory($elements[(count($elements)-1)]);
                    self::searchMainTxtFile($elements[(count($elements)-1)], $element);
                    $elements[(count($elements)-1)] = self::getHierarchy($elements[count($elements)-1]);
                }
            }
        }
    }

    /**
     * @param $path
     * @return array
     * Получает дирректории и добавляет к ним абсолютные пути
     */
    private static function addAbsolutePath($path)
    {
        $result = null;
        $elems = scandir($path);
        foreach ($elems as $elem) {
            $result[] = [$path . '/' . $elem];
        }
        return $result;
    }

    /**
     * @param $arrayPath
     * @return array
     * Удаляет базовые дирректории: '.' и '..' из массива путей дирректорий
     */
    private static function deleteBaseDirectory($arrayPath)
    {
        unset($arrayPath[0]);
        unset($arrayPath[1]);
        $arrayPath = array_values($arrayPath);
        return $arrayPath;
    }

    /**
     * @param $nameFile
     * @param $nameFolder
     * Проверяет, является ли txt файл нужным(то есть тем, который хранит описание документов)
     */
    private static function checkNameTxtFile($nameFile, $nameFolder)
    {
        $nameFolder = explode('/', $nameFolder)[count(explode('/', $nameFolder))-1];
        $nameFile = explode( '/', $nameFile)[count(explode( '/', $nameFile))-1];
        if (preg_match('/(RC_Data_)/', $nameFile)) {
            $parthFileName = explode('.', explode('RC_Data_', $nameFile)[1])[0];
        }
        return preg_match("/($parthFileName)/", $nameFolder);
    }

    /**
     * Возвращает пути файлов и запускает вторую часть приложения по созданию ЭД
     */
    private static function end()
    {
//        self::$pathToDescrFile = getcwd(). '/ED_DESCR.csv';
//        self::$pathToRelationFile = getcwd() . '/ED_Files.csv';

        self::$absPathToEdDescr = getcwd(). '/ED_DESCR.csv';
        self::$absPathToEdFiles = getcwd() . '/ED_Files.csv';
    }

    /**
     * @param $path
     * @param $url
     * @throws Exception
     * Конструктор для второй части по созданию ЭД в БД
     */
    public static function defaultEad($path, $url)
    {
//        self::$absPathToDir = $path;
//        self::$url = $url;
//        self::$absPathToEdDescr = self::$absPathToDir . '/ED_DESCR.csv';
//        self::$absPathToEdFiles = self::$absPathToDir . '/ED_Files.csv';

        self::$sourceID = self::getSource();
        self::$statusEad = self::getStatusEad();
        self::$fileRoles = self::getFilesRoles();

        self::start();
    }

    /**
     * @return Exception
     * Начинает работу по созданию ЭД в БД (на данный момент все csv файлы созданы)
     */
    public static function start()
    {
        try {
            self::$edDescr = self::readFileEdCsv(self::$absPathToEdDescr);
            self::$edFiles = self::readFileEdCsv(self::$absPathToEdFiles);

            self::parsingContentEdDescr(self::$edDescr);
            self::parsingContentEdFiles(self::$edFiles);


            self::checkRoleDocumentOnFiles();
            self::setParentFilesOnEd();

            self::createOrUpdateEad();

//            echo 'Скрипт выполнился';
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $file
     * @return false|string[]
     * Читает файл File csv и декодирует содержимое
     */
    private static function readFileEdCsv($file)
    {
        $line_file = file_get_contents($file);
        $decoded_line_file = mb_convert_encoding($line_file, 'UTF-8', 'Windows-1251'); # TODO Посмотреть нужна ли будет после смены кодировки

        //$result = explode(PHP_EOL, $decoded_line_file);
        $result = explode("\n", $decoded_line_file);
        return $result;
    }

    /**
     * @param $array
     * Парсит контент ED_DESCR.csv
     */
    private static function parsingContentEdDescr($array)
    {
        foreach ($array as $item) {
            if (!empty($item)) {
                $arr_item = explode(';', $item);
                if (count($arr_item) == 1 || count($arr_item) < 3) {
                    $arr_item = explode(',', $item);
                }

                self::$eadArray[$arr_item[0]]['num'] = $arr_item[1];
                self::$eadArray[$arr_item[0]]['reg_date'] = date("Y-m-d", strtotime($arr_item[2]));
                self::$eadArray[$arr_item[0]]['name'] = str_replace("\"", '', $arr_item[3]);
                self::$eadArray[$arr_item[0]]['source_id'] = self::$sourceID;
//                self::$eadArray[$arr_item[0]]['ed_status_id'] = self::$statusEad;
            }
        }
    }

    /**
     * @param $array
     * Парсит контент ED_Files.csv
     */
    private static function parsingContentEdFiles($array)
    {
        foreach ($array as $item) {
            if (!empty($item)) {
                $arr_item = explode(';', $item);
                if (count($arr_item) == 1 || count($arr_item) < 3) {
                    $arr_item = explode(',', $item);
                }

                $fileName = basename($arr_item[2]);
                $fileName = str_replace(['"'], '', $fileName);

                self::$eadArray[$arr_item[1]]['files'][$arr_item[0]]['path'] = str_replace('//', '/', $arr_item[2]);
                self::$eadArray[$arr_item[1]]['files'][$arr_item[0]]['file'] = $fileName;

                if (empty(self::$hasRoleFiles[$arr_item[1]])) {
                    self::$hasRoleFiles[$arr_item[1]] = false;
                }

                foreach (self::$fileRoles['file_roles'] as $role) {
                    if ($role['value'] == $arr_item[3]) {
                        self::$eadArray[$arr_item[1]]['files'][$arr_item[0]]['fr_id'] = $role['id'];

                        if ($role['id'] == self::$fileRoleTypeDocument) {
                            self::$parentFilesOnEd[$arr_item[1]]['parent_file'] = $fileName;
                            self::$hasRoleFiles[$arr_item[1]] = true;
                        }
                    }
                }
            }
        }
    }

    /**
     * Проверят роль файлов
     */
    private static function checkRoleDocumentOnFiles()
    {
        foreach (self::$hasRoleFiles as $keyRole => $role) {
            if (!$role) {
                $keyFirstFiles = array_keys(self::$eadArray[$keyRole]['files'])[0];
                self::$eadArray[$keyRole]['files'][$keyFirstFiles]['fr_id'] = self::$fileRoleTypeDocument;
                self::$parentFilesOnEd[$keyRole]['parent_file'] = self::$eadArray[$keyRole]['files'][$keyFirstFiles]['file'];
            }
        }
    }

    /**
     * Устанавливает родительский файл, то есть тот, который имеет роль документа среди остальных
     */
    private static function setParentFilesOnEd ()
    {
        foreach (self::$eadArray as $keyEad => $ead) {
            if (!empty($ead['files'])) {
                foreach ($ead['files'] as $keyFile => $file) {
                    if ($file['fr_id'] != self::$fileRoleTypeDocument) {
                        self::$eadArray[$keyEad]['files'][$keyFile]['parent_file'] = self::$parentFilesOnEd[$keyEad]['parent_file'];
                    }
                }
            }

            if (!empty(self::$eadArray[$keyEad]['files'])) {
                $filesOld = self::$eadArray[$keyEad]['files'];
                $files = array_values(self::$eadArray[$keyEad]['files']);
                unset($ead['files']);
                self::$eadArray[$keyEad]['files'] = $files;
            }
        }
    }

    /**
     * @return mixed
     * @throws Exception
     * Получает источники
     */
    private static function getSource()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$url . '/ead/nsi/sources/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

        $resp = curl_exec($ch);

        if ($e = curl_error($ch)) {
            curl_close($ch);
            throw new \Exception($e);
        }

        curl_close($ch);

        $decoded = json_decode($resp);

        foreach ($decoded->sources as $source) {
            if ($source->value === 'Массовая загрузка ЭАД') {
                return $source->id;
            }
        }
    }

    /**
     * @return mixed
     * @throws Exception
     * Получает статусы ЭД
     */
    private static function getStatusEad()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$url . '/ead/nsi/ed_statuses/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

        $resp = curl_exec($ch);

        if ($e = curl_error($ch)) {
            curl_close($ch);
            throw new \Exception($e);
        }

        curl_close($ch);

        $decoded = json_decode($resp);

        foreach ($decoded->ed_statuses as $status) {
            if ($status->value === 'Принят из загрузки') {
                self::$edStatusAccepted = $status->id;
            } elseif ($status->value === 'Обновлен из загрузки') {
                self::$edStatusUpdated = $status->id;
            }
        }
    }

    private static function getFilesRoles()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$url . '/ead/nsi/file_roles/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

        $resp = curl_exec($ch);

        if ($e = curl_error($ch)) {
            curl_close($ch);
            throw new \Exception($e);
        }

        curl_close($ch);

        $decoded = json_decode($resp, true);

        foreach ($decoded['file_roles'] as $role) {
            if ($role['value'] == 'Документ') {
                self::$fileRoleTypeDocument = $role['id'];
                break;
            }
        }

        return $decoded;
    }

    /**
     * @throws Exception
     * Создает или обновляет ЭД
     */
    private static function createOrUpdateEad()
    {
        foreach (self::$eadArray as $ead) {
            $data = self::searchEad($ead);

            if (count($data['eds']) > 0) {
                $ead['ed_status_id'] = self::$edStatusUpdated;
                self::updateEadPut($ead, $data['eds']);
            } else {
                $ead['ed_status_id'] = self::$edStatusAccepted;
                self::createEadPost($ead);
            }
        }
    }

    /**
     * @param $ed
     * @return mixed|null
     * @throws Exception
     * Ищет в БД Электронный документ по номеру и регистрационной дате
     */
    private static function searchEad($ed)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$url . "/ead/eds/?num={$ed['num']}&reg_date={$ed['reg_date']}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

        $resp = curl_exec($ch);

        if ($e = curl_error($ch)) {
            curl_close($ch);
            throw new \Exception($e);
        }

        curl_close($ch);

        $decoded = json_decode($resp, true);

        if (!empty($decoded)) {
            return $decoded;
        } else {
            return null;
        }
    }

    /**
     * @param $ed
     * @param $data
     * Обновляет существующий ЭД документ в БД
     */
    private static function updateEadPut ($ed, $data)
    {
        if (!empty($ed['files']) && count($ed['files']) > 0) {
            $strFiles = '';
            foreach ($ed['files'] as $key => $file) {
                $path = str_replace('"', '', $file['path']);
                //$strFiles .= "--form 'files[]=@\"{$path}\"' ";
                //$strFiles .= "--form " . "'files[]=@" . "\"" . $path . "\"" . " ' ";
                // $strFiles .= '--form ' . 'files[]=@ ' . "$path";
                $strFiles .= '--form "files[]=@\"'.$path.'\"" ';

                unset($ed['files'][$key]['path']);
            }
            $ed['id'] = $data[0]['id'];
            $curlUrl = self::$url . "/ead/eds/{$data[0]['id']}";
            $jsonData = json_encode($ed);
            $jsonData = str_replace('"', '\"', $jsonData);

            // $curlCommand = "curl POST" . " \"$curlUrl\" " . "--header " . "'Content-Type: multipart/form-data' " . $strFiles;
            // $curlCommand .= "--form 'data=" . "\"$jsonData\";type=application/json' --form '_method=\"PUT\"'";

            $curlCommand = 'curl POST "'.$curlUrl.'" --header "Content-Type: multipart/form-data" ';
            $curlCommand .= $strFiles.'--form "data='.$jsonData.';type=application/json" --form "_method=PUT"';

            $result = shell_exec($curlCommand);

            $response = json_decode($result);

            if (!empty($response->code) && ($response->code == 200 || $response->code == 201)) {
                $type = 'INFO';
            } else {
                $type = 'ERROR';
                if (!empty($response)) {
                    $response->message = json_encode($response->message, JSON_UNESCAPED_UNICODE);
                }
            }

            if (!empty($response)) {
                $string = "code: {$response->code}, message: {$response->message}."."\n";
                $string .= ' Данные, которые отправили: '. json_encode($ed, JSON_UNESCAPED_UNICODE);
            } else {
                $string = 'Response: null' . $response;
            }

            self::writeLogs($string, $type);
        }
    }


    private static function createEadPost ($ed)
    {
        if (!empty($ed['files']) && count($ed['files']) > 0) {
            $strFiles = '';
            foreach ($ed['files'] as $key => $file) {
                $path = str_replace('"', '', $file['path']);
                // $strFiles .= "--form 'files[]=@\"{$path}\"' ";
                $strFiles .= '--form "files[]=@\"'.$path.'\"" ';

                unset($ed['files'][$key]['path']);
            }

            $curlUrl = self::$url . '/ead/eds/';
            $jsonData = json_encode($ed);
            $jsonData = str_replace('"', '\"', $jsonData);

            // $curlCommand = "curl POST" . " \"$curlUrl\" " . "--header " . "'Content-Type: multipart/form-data' " . $strFiles;
            // $curlCommand .= "--form 'data=" . "\"$jsonData\";type=application/json'";

            //$curlCommand = "curl POST . \"$curlUrl\" . --header 'Content-Type: multipart/form-data' {$strFiles}";
            //$curlCommand .= "--form 'data=\"$jsonData\";type=application/json'";

            $curlCommand = 'curl POST "'.$curlUrl.'" --header "Content-Type: multipart/form-data" ';
            $curlCommand .= $strFiles.'--form "data='.$jsonData.';type=application/json"';

            $result = shell_exec($curlCommand);

            $response = json_decode($result);

            if (!empty($response->code) && ($response->code == 200 || $response->code == 201)) {
                $type = 'INFO';
            } else {
                $type = 'ERROR';
                if (!empty($response)) {
                    $response->message = json_encode($response->message, JSON_UNESCAPED_UNICODE);
                }
            }

            if (!empty($response)) {
                $string = "code: {$response->code}, message: {$response->message}."."\n";
                $string .= ' Данные, которые отправили: '. json_encode($ed, JSON_UNESCAPED_UNICODE);
            } else {
                $string = 'Response: null' . $response;
            }

            self::writeLogs($string, $type);
        }
    }
}